# -*- coding: utf-8 -*-

# Take a wild guess.
INPUT_DIRECTORY = 'input/'
OUTPUT_DIRECTORY = 'output/'

# Directories to be copied straight from INPUT_DIRECTORY
# to OUTPUT_DIRECTORY.
COPY_DIRS = (
    'css/',
    'js/',
    'img/'
)

# Same as above, but files.
COPY_FILES = (
    'favicon.ico',
)

# A dictionary of tuples of all templates, keyed by
# directory.
TEMPLATES = {
    '': (
        'index.htm',
        'about.htm',
    ),
    'people/': (
        'ceo.htm',
        'intern.htm',
        'secretary.htm',
        'janitor.htm',
        'office-jester.htm',
    )
}

# Template variables used by any or all of your templates. Best
# used by only your layout, but you can use them for whatever
# you want.
TEMPLATE_VARS = {
    'title': 'My Site',
    'header': 'Hello, world!',
    'css_url': 'http://mysite.com/css/',
    'js_url': 'http://mysite.com/js/',
    'img_url': 'http://mysite.com/img/',
}