# -*- coding: utf-8 -*-

import shutil
import errno
import config
import os

from jinja2 import Environment, FileSystemLoader

# Single function for copying directory trees or single files.
# Asks forgiveness instead of permission. Apparently that's whoat
# you're supposed to do.
def copy(src, dst):
    try:
        shutil.copytree(src, dst)
    except OSError as exc:
        if exc.errno == errno.ENOTDIR or exc.errno == errno.EINVAL:
            shutil.copy(src, dst)
        else: raise

# Copy a sequence of things, which can be directories or files.
def copy_things(sources, in_dir, out_dir):
    for s in sources:
        print 'copy: ' + s
        copy(in_dir+s, out_dir+s)

# Process a list of templates, with a global list of template
# variables to apply to them.
def process_templates(templates, in_dir, out_dir, template_vars={}):
    env = Environment(loader=FileSystemLoader(in_dir))
    for t_dir in templates:
        if not os.path.isdir(out_dir+t_dir):
            print 'create: ' + t_dir
            os.makedirs(out_dir+t_dir)
        for t_file in templates[t_dir]:
            t_file = t_dir + t_file
            print 'gen: ' + t_file
            template = env.get_template(t_file)
            rendered = template.render(**template_vars)
            with open(out_dir+t_file, 'w') as f:
                f.write(rendered)

if __name__ == '__main__':
    shutil.rmtree(config.OUTPUT_DIRECTORY)
    os.mkdir(config.OUTPUT_DIRECTORY)
    copy_things(config.COPY_DIRS, config.INPUT_DIRECTORY, config.OUTPUT_DIRECTORY)
    copy_things(config.COPY_FILES, config.INPUT_DIRECTORY, config.OUTPUT_DIRECTORY)
    process_templates(config.TEMPLATES, config.INPUT_DIRECTORY, config.OUTPUT_DIRECTORY, config.TEMPLATE_VARS)