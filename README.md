## What is this thing?

sitegen is a stupidly simple Jinja2-based static site generator. Unlike most static site generators, this one offers
no fancy features. It just takes in Jinja2 templates and spits out HTML. I wrote it to manage a small static site I
use and found it tedious to learn and setup other static site generators.

If you need really advanced features, sitegen is not for you. If you need nothing more than to just insert templates
and receive HTML, and copy your static files to a different directory, you've come to the right place. I'm not even
joking, this thing doesn't do anything else at all.

## First things first...

There are bugs. Unicode doesn't work properly, and sometimes creating the output directory throws an exception.
If you want to fix them, please feel free to do so and I will merge your changes. I just wanted to bang this thing out
real quick so I could manage a small static site, and I didn't need Unicode nor did I mind having to manually create
a directory sometimes.

## So how do I use it?

Just create a `config.py` with your desired settings. The included example shows every available option. They are all
required because I didn't feel like adding a bunch of error checking (like I said, I wanted this thing *right now*),
but some can, of course, be blank.

Now you write your templates and stick them wherever you want in your `config.INPUT_DIRECTORY`. If you're familiar with
Jinja2 or Django, everything works just like that. There should be no surprises here. Be sure to update `config.TEMPLATES`
when you add or remove a template.

Keep in mind that the config file is just Python. You can do anything you want, such as automatically generate build dates
for your website or make conditional settings (e.g. for making separate debug and deployment versions).

Then you simply run `sitegen.py`. Done and done. Upload away.